package com.github.prusya26.toolstelegrambot;

import com.github.prusya26.toolstelegrambot.data.model.VariableDateFormat;
import com.github.prusya26.toolstelegrambot.data.repository.VariableDateFormatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.generics.BotSession;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;

@Slf4j
@Component
public class BotComponent extends AbilityBot {

    private final TelegramBotsApi botsApi;
    private final MainProperties mainProperties;
    private final DateService dateService;
    private final CommandsHelpService commandsHelpService;
    private final MessageParser messageParser;
    private final VariableDateFormatRepository variableDateFormatRepository;

    private BotSession toolsBotSession;

    @Autowired
    public BotComponent(TelegramBotsApi botsApi,
                        MainProperties mainProperties,
                        DateService dateService,
                        CommandsHelpService commandsHelpService,
                        MessageParser messageParser,
                        VariableDateFormatRepository variableDateFormatRepository) {
        super(mainProperties.getBotSettings().getToken(), mainProperties.getBotSettings().getUsername());
        this.botsApi = botsApi;
        this.mainProperties = mainProperties;
        this.dateService = dateService;
        this.commandsHelpService = commandsHelpService;
        this.messageParser = messageParser;
        this.variableDateFormatRepository = variableDateFormatRepository;
    }

    @PostConstruct
    protected void init() {
        try {
            toolsBotSession = botsApi.registerBot(this);
            log.info("Tools bot started");
        } catch (TelegramApiRequestException e) {
            log.error("Quiz bot registration error", e);
        }
    }

    @PreDestroy
    protected void onStop() {
        if(toolsBotSession.isRunning()) {
            toolsBotSession.stop();
        }
    }

    @Override
    public int creatorId() {
        return mainProperties.getBotSettings().getCreatorId();
    }

    public Ability uuid() {
        return Ability.builder()
                .name("uuid")
                .info("generate new UUID")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send(UUID.randomUUID().toString(), ctx.chatId()))
                .build();
    }

    public Ability help() {
        return Ability.builder()
                .name("help")
                .info("show commands list")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(reportCommands().action())
                .post(reportCommands().postAction())
                .build();
    }

    public Ability millisToDate() {
        return Ability.builder()
                .name("millistodate")
                .info("Convert milliseconds to date. /man millistodate for help")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> {
                    try {
                        List<String> arguments = getArguments(ctx);
                        String formatString = getSecondArg(arguments);
                        long millis = Long.valueOf(getFirstArg(arguments));
                        if(formatString == null || formatString.isEmpty()) {
                            formatString = getUserDateFormat(ctx.user().id());
                        }
                        String result = null;
                        if(formatString != null && !formatString.isEmpty()) {
                           result = dateService.millisToDateString(millis, formatString);
                        }
                        if(result == null) {
                            result = new Date(millis).toString();
                        }
                        silent.send(result, ctx.chatId());
                    } catch (Exception e) {
                        silent.send("Conversion error: " + e.getMessage(), ctx.chatId());
                    }
                })
                .build();
    }

    public Ability dateToMillis() {
        return Ability.builder()
                .name("datetomillis")
                .info("Convert date string to milliseconds. /man datetomillis for help")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> {
                    List<String> arguments = getArguments(ctx);
                    String dateString = getFirstArg(arguments);
                    String formatString = getSecondArg(arguments);
                    if(dateString != null && !dateString.isEmpty()) {
                        long millis;
                        if(formatString != null && !formatString.isEmpty()) {
                            millis = dateService.dateStringToMillis(dateString, formatString);
                        } else {
                            formatString = getUserDateFormat(ctx.user().id());
                            if(formatString != null && !formatString.isEmpty()) {
                                millis = dateService.dateStringToMillis(dateString, formatString);
                            } else {
                                millis = dateService.dateStringToMillis(dateString);
                            }
                        }
                        if(millis > 0) {
                            silent.send(String.valueOf(millis), ctx.chatId());
                        } else {
                            silent.send("Invalid date string format", ctx.chatId());
                        }
                    } else {
                        silent.send("Date string must be specified at second argument", ctx.chatId());
                    }
                })
                .build();
    }

    public Ability setCommand() {
        return Ability.builder()
                .name("set")
                .info("Set bot variable value. /man set for help")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> {
                    List<String> arguments = getArguments(ctx);
                    String variableName = getFirstArg(arguments);
                    String value = getSecondArg(arguments);
                    if(variableName != null && !variableName.isEmpty() && value != null && !value.isEmpty()) {
                        variableName = variableName.toUpperCase();
                        if(variableName.equals("DATE_FORMAT")) {
                            //выполним проверку на правильность формата
                            try {
                                SimpleDateFormat format = new SimpleDateFormat(value);
                                format.format(new Date());
                                storeUserDateFormat(ctx.user().id(), value);
                                silent.send("Value for variable DATE_FORMAT is successfully set", ctx.chatId());
                            } catch (Exception e) {
                                silent.send("Invalid date format.", ctx.chatId());
                            }
                        }
                    } else {
                        silent.send("You must specify variable name and it's value.", ctx.chatId());
                    }
                })
                .build();
    }

    public Ability manCommand() {
        return Ability.builder()
                .name("man")
                .info("Show usage information for command")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> {
                    List<String> arguments = getArguments(ctx);
                    String commandName = getFirstArg(arguments);
                    if(commandName != null && !commandName.isEmpty()) {
                        silent.send(commandsHelpService.getHelpForCommand(commandName), ctx.chatId());
                    } else {
                        silent.send("You must specify command name as first argument of command /man", ctx.chatId());
                    }
                })
                .build();
    }

    private List<String> getArguments(MessageContext ctx) {
        String text = ctx.update().getMessage().getText();
        return messageParser.arguments(text);
    }

    private String getFirstArg(List<String> arguments) {
        if(arguments.size() >= 1) return arguments.get(0);
        else return null;
    }

    private String getSecondArg(List<String> arguments) {
        if(arguments.size() >= 2) return arguments.get(1);
        else return null;
    }

    private String getUserDateFormat(int userId) {
        VariableDateFormat variable = variableDateFormatRepository.findOne(userId);
        if(variable != null) return variable.getValue();
        else return null;
    }

    private void storeUserDateFormat(int userId, String value) {
        VariableDateFormat variableDateFormat = variableDateFormatRepository.findOne(userId);
        if(variableDateFormat == null)
            variableDateFormat = VariableDateFormat.builder()
                    .userId(userId)
                    .build();
        variableDateFormat.setValue(value);
        variableDateFormatRepository.save(variableDateFormat);
    }
}
