package com.github.prusya26.toolstelegrambot;

import org.springframework.stereotype.Service;

@Service
public class CommandsHelpService {
    public String getHelpForCommand(String commandName) {
        switch (commandName.toLowerCase()) {
            case "millistodate":
                return millisToDateHelp();
            default:
                return "no such command";
        }
    }

    private String millisToDateHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("usage:\n")
                .append("\t/millistodate milliseconds [format_string]\n")
                .append("arguments:\r")
                .append("\tmilliseconds - number of milliseconds since the standard base time known as \"the epoch\", namely January 1, 1970, 00:00:00 GMT.")
                .append("\n")
                .append("\tformat_string - date and time pattern string");
        return sb.toString();
    }

    private String dateToMillisHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("usage:\n")
                .append("\t/datetomillis date_string [format_string]\n")
                .append("arguments:\r")
                .append("\tdate_string - date in string representation")
                .append("\n")
                .append("\tformat_string - date and time pattern string");
        return sb.toString();
    }
}
