package com.github.prusya26.toolstelegrambot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DateService {

    private final List<SimpleDateFormat> defaultFormats;
    private final MainProperties mainProperties;

    @Autowired
    public DateService(MainProperties mainProperties) {
        this.mainProperties = mainProperties;
        defaultFormats = mainProperties.getDateConverter().getFormats().stream()
                .map(SimpleDateFormat::new)
                .collect(Collectors.toList());
    }

    public long dateStringToMillis(String dateString) {
        for (SimpleDateFormat format : defaultFormats) {
            try {
                Date date = format.parse(dateString);
                return date.getTime();
            } catch (ParseException e) {
            }
        }
        return -1;
    }

    public long dateStringToMillis(String dateString, String formatString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            Date date = format.parse(dateString);
            return date.getTime();
        } catch (NullPointerException | IllegalArgumentException | ParseException e) {
            return dateStringToMillis(dateString);
        }
    }

    public String millisToDateString(long millis, String formatString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            return format.format(new Date(millis));
        } catch (NullPointerException | IllegalArgumentException e) {
            return null;
        }
    }
}
