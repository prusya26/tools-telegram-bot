package com.github.prusya26.toolstelegrambot;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties("com.github.prusya26.toolstelegrambot")
public class MainProperties {

    private BotSettings botSettings;
    private DateConverter dateConverter;

    @Data
    public static class BotSettings {
        private String token;
        private String username;
        private int creatorId;
    }

    @Data
    public static class DateConverter {
        List<String> formats;
    }
}
