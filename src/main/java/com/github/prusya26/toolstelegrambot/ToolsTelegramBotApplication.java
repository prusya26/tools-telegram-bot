package com.github.prusya26.toolstelegrambot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;

@SpringBootApplication
public class ToolsTelegramBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToolsTelegramBotApplication.class, args);
	}

	@Bean
	public TelegramBotsApi botsApi() {
		ApiContextInitializer.init();
		return new TelegramBotsApi();
	}
}
