package com.github.prusya26.toolstelegrambot.data.repository;

import com.github.prusya26.toolstelegrambot.data.model.VariableDateFormat;
import org.springframework.data.repository.CrudRepository;

public interface VariableDateFormatRepository extends CrudRepository<VariableDateFormat, Integer> {

}
